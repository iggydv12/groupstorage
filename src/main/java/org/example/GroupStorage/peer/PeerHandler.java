package org.example.GroupStorage.peer;

import com.google.common.util.concurrent.SettableFuture;
import io.netty.bootstrap.ServerBootstrap;
import io.netty.channel.*;
import io.netty.channel.nio.NioEventLoopGroup;
import io.netty.channel.socket.nio.NioServerSocketChannel;
import io.netty.handler.codec.serialization.ObjectEncoder;
import io.netty.handler.logging.LogLevel;
import io.netty.handler.logging.LoggingHandler;
import org.example.GroupStorage.peer.Handlers.PeerChannelHandler;
import org.example.GroupStorage.peer.config.Configuration;
import org.example.GroupStorage.peer.network.PeerChannelInitializer;
import org.example.GroupStorage.peer.service.ConnectionService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.concurrent.CompletableFuture;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;

import static java.util.concurrent.TimeUnit.SECONDS;


public class PeerHandler {
    private final static Logger logger = LoggerFactory.getLogger(PeerHandler.class);

    private final Configuration configuration;
    private final int port;
    private final ObjectEncoder encoder = new ObjectEncoder();
    private final Peer peer;

    private Future keepAliveFuture;
    private Future timeoutPingsFuture;

    private final EventLoopGroup peerEventLoopGroup = new NioEventLoopGroup(1);
    private final EventLoopGroup bossGroup = new NioEventLoopGroup(1);
    private final EventLoopGroup workerGroup = new NioEventLoopGroup(6);

    public PeerHandler(Configuration configuration, int port) {
        this.configuration = configuration;
        this.port = port;
        final ConnectionService connectionService = new ConnectionService(configuration, workerGroup, peerEventLoopGroup, encoder);
        this.peer = new Peer(configuration, connectionService);

    }

    public ChannelFuture run() throws Exception {
        ChannelFuture closeFuture = null;

        PeerChannelHandler peerChannelHandler = new PeerChannelHandler(configuration, peer);
        PeerChannelInitializer peerChannelInitializer = new PeerChannelInitializer(configuration, encoder, peerEventLoopGroup, peerChannelHandler);
        ServerBootstrap bootstrap = new ServerBootstrap();
        bootstrap.group(bossGroup, workerGroup)
                .channel(NioServerSocketChannel.class)
                .option(ChannelOption.SO_BACKLOG, 128)
                .option(ChannelOption.CONNECT_TIMEOUT_MILLIS, 10000)
                .option(ChannelOption.SO_KEEPALIVE, true)
                .handler(new LoggingHandler(LogLevel.INFO))
                .childHandler(peerChannelInitializer);

        // Bind and start to accept incoming connections.
        ChannelFuture channelFutureBind = bootstrap.bind(port).sync();

        if (channelFutureBind.isSuccess()) {
            logger.info("{} Binding to {} Successful!", configuration.getNode().getPeerName(), port);
            Channel serverChannel = channelFutureBind.channel();

            final SettableFuture<Void> setServerChannelFuture = SettableFuture.create();
            peerEventLoopGroup.execute(() -> {
                try {
                    peer.setChannel(serverChannel);
                    setServerChannelFuture.set(null);
                } catch (Exception e) {
                    logger.error("Unable to set server channel");
                    setServerChannelFuture.setException(e);
                }
            });

            try {
                setServerChannelFuture.get(10, TimeUnit.SECONDS);
            } catch (Exception e) {
                logger.error("Couldn't set bind channel to server {}: {}", configuration.getNode().getPeerName(), e);
                peerEventLoopGroup.shutdownGracefully();
                workerGroup.shutdownGracefully();
                bossGroup.shutdownGracefully();
            }

            keepAliveFuture = peerEventLoopGroup.scheduleAtFixedRate((Runnable) peer::keepAlivePing, 1, configuration.getNode().getKeepAlive(), SECONDS);
            //timeoutPingsFuture = peerEventLoopGroup.scheduleAtFixedRate((Runnable) peer::timeoutPings, 1, 100, TimeUnit.MILLISECONDS);
            closeFuture = serverChannel.closeFuture();
        } else {
            logger.error("{} could not bind to {}: {}", configuration.getNode().getPeerName(), port, channelFutureBind.cause());
        }

        return closeFuture;
    }


    public CompletableFuture<Void> connect(final String host, final int port) {
        logger.info("Trying to connect {}:{}", host, port);
        final CompletableFuture<Void> connectToHostFuture = new CompletableFuture<>();
        peerEventLoopGroup.execute(() -> peer.connectTo(host, port, connectToHostFuture));
        logger.info("Completing connect request {}", connectToHostFuture.toString());
        return connectToHostFuture;
    }

    public void disconnect(final String peerName) {
        //peerEventLoopGroup.execute(() -> peer.disconnect(peerName));
    }
}
