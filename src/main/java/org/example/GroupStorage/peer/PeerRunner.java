package org.example.GroupStorage.peer;

import io.netty.channel.ChannelFuture;
import org.example.GroupStorage.peer.config.Configuration;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.function.BiConsumer;

public class PeerRunner {
    private static final Logger logger = LoggerFactory.getLogger(PeerRunner.class);
    private PeerHandler peerHandler;

    public enum CommandResult {
        CONTINUE,
        SHUT_DOWN,
        INVALID_COMMAND
    }

    public PeerRunner(final Configuration configuration, final int port) {
        peerHandler = new PeerHandler(configuration, port);
    }

    public ChannelFuture run() throws InterruptedException {
        try {
            logger.info("Running!");
            return peerHandler.run();
        } catch (Exception e) {
            logger.error("Something went wrong");
        }
        return null;
    }

    public CommandResult handleCommand(final String command) {
        CommandResult result = CommandResult.CONTINUE;
        try {
            if (command.equals("ping")) {
                logger.info("Ping command");
            } else if (command.equals("leave")) {
                logger.info("Leave command");
                result = CommandResult.SHUT_DOWN;
            } else if (command.startsWith("connect ")) {
                final String[] tokens = command.split(" ");
                final String hostToConnect = tokens[1];
                final int portToConnect = Integer.parseInt(tokens[2]);
                logger.info("Attempting to connect to {}:{}", hostToConnect, portToConnect);
                peerHandler.connect(hostToConnect, portToConnect).whenComplete(new ConnectFutureListener(hostToConnect, portToConnect));
            } else if (command.startsWith("disconnect ")) {
                final String[] tokens = command.split(" ");
                peerHandler.disconnect(tokens[1]);
            } else if (command.equals("election")) {
                logger.info("Election called");
                //peerHandler.scheduleLeaderElection();
            } else {
                result = CommandResult.INVALID_COMMAND;
            }
        } catch (Exception e) {
            logger.error("Command failed: " + command, e);
            result = CommandResult.INVALID_COMMAND;
        }

        return result;
    }

    private static class ConnectFutureListener implements BiConsumer<Void, Throwable> {

        private final String hostToConnect;
        private final int portToConnect;

        public ConnectFutureListener(String hostToConnect, int portToConnect) {
            this.hostToConnect = hostToConnect;
            this.portToConnect = portToConnect;
        }

        @Override
        public void accept(Void aVoid, Throwable throwable) {
            if (throwable == null) {
                logger.info("Successfully connected to {}:{}", hostToConnect, portToConnect);
            } else {
                logger.error("Connection to {}:{} failed! Cause: {}", hostToConnect, portToConnect, throwable.getCause());
            }
        }
    }

}
