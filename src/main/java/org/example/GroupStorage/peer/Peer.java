package org.example.GroupStorage.peer;


import io.netty.channel.*;
import org.example.GroupStorage.peer.config.Configuration;
import org.example.GroupStorage.peer.network.Connection;
import org.example.GroupStorage.peer.service.ConnectionService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.concurrent.CompletableFuture;


public class Peer {
    private static final Logger logger = LoggerFactory.getLogger(Peer.class);

    private final Configuration configuration;
    private boolean running = true;
    private final ConnectionService connectionService;
    private Channel channel;

    public Peer(Configuration configuration, ConnectionService connectionService) {
        this.configuration = configuration;
        this.connectionService = connectionService;
    }


    public void handleConnectionOpened(Connection connection, String leaderName) {
        if (isShutdown()) {
            logger.warn("New connection of {} ignored since not running", connection.getPeerName());
            return;
        }

        if (connection.getPeerName().equals(configuration.getNode().getPeerName())) {
            logger.error("Can't connect to itself. Closing new connection.");
            connection.close();
            return;
        }

        connectionService.addConnection(connection);
//        if (leaderName != null) {
//            //final String currentLeaderName = leadershipService.getLeaderName();
//            if (currentLeaderName == null) {
//                leadershipService.handleLeader(connection, leaderName);
//            } else if (!leaderName.equals(currentLeaderName)) {
//                logger.info("Known leader {} and leader {} announced by {} are different.", currentLeaderName, leaderName, connection);
//                leadershipService.scheduleElection();
//            }
//        }
        //pingService.propagatePingsToNewConnection(connection);
    }

    public void handleConnectionClosed(Connection connection) {
        if (connection == null) {
            return;
        }

        final String connectionPeerName = connection.getPeerName();
        if (connectionPeerName == null || connectionPeerName.equals(configuration.getNode().getPeerName())) {
            return;
        }

        if (connectionService.removeConnection(connection)) {
            logger.info("pings cancelled");
//            cancelPings(connection, connectionPeerName);
//            cancelPongs(connectionPeerName);
        }

//        if (connectionPeerName.equals(leadershipService.getLeaderName())) {
//            LOGGER.warn("Starting an election since connection to current leader {} is closed.", connectionPeerName);
//            leadershipService.scheduleElection();
//        }
    }

    public void keepAlivePing() {
        if (isShutdown()) {
            logger.warn("Periodic ping ignored since not running");
        }
    }

    public void timeoutPings() {
        if (isShutdown()) {
            logger.warn("Timeout pings ignored since not running");
        }
    }


    public void connectTo(final String host, final int port, final CompletableFuture<Void> futureToNotify) {
        if (running) {
            connectionService.connectTo(this, host, port, futureToNotify);
        } else {
            futureToNotify.completeExceptionally(new RuntimeException("Server is not running"));
        }
    }

    private boolean isShutdown() {
        return !running;
    }

    public Configuration getConfiguration() {
        return configuration;
    }

    public Channel getChannel() {
        return channel;
    }

    public void setChannel(Channel channel) {
        this.channel = channel;
    }

}
