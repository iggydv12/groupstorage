package org.example.GroupStorage.peer.config;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.dataformat.yaml.YAMLFactory;

import java.io.IOException;
import java.io.InputStream;

public class ConfigurationReader {

    public static Configuration loadProperties() throws IOException {
        String filename = "application.yml";
        InputStream inputStream = Thread.currentThread().getContextClassLoader().getResourceAsStream(filename);
        ObjectMapper mapper = new ObjectMapper(new YAMLFactory());
        return mapper.readValue(inputStream, Configuration.class);
    }
}
