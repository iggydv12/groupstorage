package org.example.GroupStorage.peer.config;

import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.IOException;

@Data
@NoArgsConstructor(force = true)
public class Configuration {
    NodeConfiguration node;

    private static Configuration configuration;

    public static Configuration initialize() throws IOException {
        configuration = ConfigurationReader.loadProperties();
        return configuration;
    }

    public static Configuration get() {
        return configuration;
    }
}
