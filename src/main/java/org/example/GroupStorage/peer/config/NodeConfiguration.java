package org.example.GroupStorage.peer.config;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor(force = true)
public class NodeConfiguration {
    private int minNumberActiveConnections;
    private int maxReadIdle;
    private int keepAlive;
    private int pingTimeout;
    private int autoDiscoveryPingFrequency;
    private int pingTTL;
    private int leaderElectionTimeout;
    private int leasderRejectionTimeout;
    private String peerName;
}
