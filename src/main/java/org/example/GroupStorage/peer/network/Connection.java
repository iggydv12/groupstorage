package org.example.GroupStorage.peer.network;

import io.netty.channel.ChannelHandlerContext;
import org.example.GroupStorage.peer.network.messages.Message;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.net.InetSocketAddress;

public class Connection {
    private static final Logger logger = LoggerFactory.getLogger(Connection.class);

    private final InetSocketAddress remoteAddress;
    private ChannelHandlerContext handlerContext;
    private String peerName;

    public Connection(ChannelHandlerContext ctx) {
        this.remoteAddress = (InetSocketAddress) ctx.channel().remoteAddress();
        this.handlerContext = ctx;
    }

    public InetSocketAddress getRemoteAddress() {
        return remoteAddress;
    }

    public String getPeerName() {
        return peerName;
    }

    public void setPeerName(final String peerName) {
        if (this.peerName == null) {
            this.peerName = peerName;
        } else {
            logger.warn("peer name {} set again for connection {}", peerName, this);
        }
    }

    public void send(final Message msg) {
        if (handlerContext != null) {
            handlerContext.writeAndFlush(msg);
        } else {
            logger.error("Unable to send message {} to {}", msg.getClass(), toString());
        }
    }

    public void close() {
        logger.debug("Closing session of {}", toString());
        if (handlerContext != null) {
            handlerContext.close();
            handlerContext = null;
        }
    }
}
