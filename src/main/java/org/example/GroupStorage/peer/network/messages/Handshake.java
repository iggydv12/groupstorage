package org.example.GroupStorage.peer.network.messages;

import org.example.GroupStorage.peer.Peer;
import org.example.GroupStorage.peer.network.Connection;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class Handshake implements Message {
    private static Logger logger = LoggerFactory.getLogger(Handshake.class);

    private final String peerName;
    private final String leaderName;

    public Handshake(String peerName, String leaderName) {
        this.peerName = peerName;
        this.leaderName = leaderName;
    }

    @Override
    public void handle(Peer peer, Connection connection) {
        final String peerName = connection.getPeerName();
        if (peerName == null) {
            connection.setPeerName(this.peerName);
            peer.handleConnectionOpened(connection, leaderName);
        } else if (!peerName.equals(this.peerName)) {
            logger.warn(
                    "Mismatching peer name received from connection! Existing: {} Received: {}",
                    this.peerName, this.peerName);
        }
    }
}
