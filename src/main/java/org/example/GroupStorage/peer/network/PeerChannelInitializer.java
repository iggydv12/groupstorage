package org.example.GroupStorage.peer.network;

import io.netty.channel.ChannelInitializer;
import io.netty.channel.ChannelPipeline;
import io.netty.channel.socket.SocketChannel;
import io.netty.handler.codec.serialization.ClassResolvers;
import io.netty.handler.codec.serialization.ObjectDecoder;
import io.netty.handler.codec.serialization.ObjectEncoder;
import io.netty.handler.timeout.IdleStateHandler;
import io.netty.util.concurrent.EventExecutorGroup;
import org.example.GroupStorage.peer.Handlers.PeerChannelHandler;
import org.example.GroupStorage.peer.config.Configuration;

public class PeerChannelInitializer extends ChannelInitializer<SocketChannel> {

    private final Configuration configuration;
    private final ObjectEncoder encoder;
    private final EventExecutorGroup peerChannelHandlerExecutorGroup;
    private final PeerChannelHandler peerChannelHandler;

    public PeerChannelInitializer(Configuration configuration,
                                  ObjectEncoder encoder,
                                  EventExecutorGroup peerChannelHandlerExecutorGroup,
                                  PeerChannelHandler peerChannelHandler) {
        this.configuration = configuration;
        this.encoder = encoder;
        this.peerChannelHandlerExecutorGroup = peerChannelHandlerExecutorGroup;
        this.peerChannelHandler = peerChannelHandler;
    }
    @Override
    protected void initChannel(SocketChannel ch) throws Exception {
        final ChannelPipeline pipeline = ch.pipeline();

        pipeline.addLast(new ObjectDecoder(ClassResolvers.cacheDisabled(null)));
        pipeline.addLast(encoder);
        pipeline.addLast(new IdleStateHandler(configuration.getNode().getMaxReadIdle(), 0, 0));

        pipeline.addLast(peerChannelHandlerExecutorGroup, peerChannelHandler);
    }
}
