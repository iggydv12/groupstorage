package org.example.GroupStorage.peer.network.messages;

import org.example.GroupStorage.peer.Peer;
import org.example.GroupStorage.peer.network.Connection;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class KeepAlive implements Message{
    private static final Logger LOGGER = LoggerFactory.getLogger(KeepAlive.class);

    private static final long serialVersionUID = -4998803925489492616L;

    @Override
    public void handle(Peer peer, Connection connection) {
        LOGGER.debug("Keep alive ping received from {}", connection);
    }
}
