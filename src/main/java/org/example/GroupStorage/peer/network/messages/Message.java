package org.example.GroupStorage.peer.network.messages;

import org.example.GroupStorage.peer.Peer;
import org.example.GroupStorage.peer.network.Connection;

import java.io.Serializable;

public interface Message extends Serializable {

    void handle(Peer peer, Connection connection);
}
