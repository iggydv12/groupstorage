package org.example.GroupStorage.peer.Handlers;

import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.SimpleChannelInboundHandler;
import io.netty.handler.timeout.IdleState;
import io.netty.handler.timeout.IdleStateEvent;
import io.netty.util.Attribute;
import io.netty.util.AttributeKey;
import org.example.GroupStorage.peer.Peer;
import org.example.GroupStorage.peer.config.Configuration;
import org.example.GroupStorage.peer.network.Connection;
import org.example.GroupStorage.peer.network.messages.Handshake;
import org.example.GroupStorage.peer.network.messages.Message;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Handles a Peer channel.
 */
public class PeerChannelHandler extends SimpleChannelInboundHandler<Message> {

    private final static Logger logger = LoggerFactory.getLogger(PeerChannelHandler.class);
    static final String SESSION_ATTRIBUTE_KEY = "session";

    private Configuration configuration;
    private Peer peer;

    public PeerChannelHandler(Configuration configuration, Peer peer) {
        this.configuration = configuration;
        this.peer = peer;
    }

    static Attribute<Connection> getSessionAttribute(ChannelHandlerContext ctx) {
        return ctx.attr(AttributeKey.<Connection>valueOf(SESSION_ATTRIBUTE_KEY));
    }

    @Override
    protected void channelRead0(ChannelHandlerContext channelHandlerContext, Message message) throws Exception {
        logger.debug("Message {} received from {}", message.getClass(), channelHandlerContext.channel().remoteAddress());
        final Connection connection = getSessionAttribute(channelHandlerContext).get();
        message.handle(peer, connection);
    }

    @Override
    public void channelActive(final ChannelHandlerContext ctx) throws Exception {
        logger.debug("Channel active {}", ctx.channel().remoteAddress());
        final Connection connection = new Connection(ctx);
        getSessionAttribute(ctx).set(connection);
        ctx.writeAndFlush(new Handshake(configuration.getNode().getPeerName(), null));
    }

    @Override
    public void channelInactive(final ChannelHandlerContext ctx) throws Exception {
        logger.debug("Channel inactive {}", ctx.channel().remoteAddress());
        final Connection connection = getSessionAttribute(ctx).get();
        peer.handleConnectionClosed(connection);
    }

    @Override
    public void exceptionCaught(final ChannelHandlerContext ctx, final Throwable cause) {
        logger.error("Channel failure " + ctx.channel().remoteAddress(), cause);
        ctx.close();
        peer.handleConnectionClosed(getSessionAttribute(ctx).get());
    }

    @Override
    public void channelReadComplete(final ChannelHandlerContext ctx) {
        ctx.flush();
    }

    @Override
    public void userEventTriggered(final ChannelHandlerContext ctx, final Object evt) {
        if (evt instanceof IdleStateEvent) {
            final IdleStateEvent idleStateEvent = (IdleStateEvent) evt;
            if (idleStateEvent.state() == IdleState.READER_IDLE) {
                logger.warn("Channel idle {}", ctx.channel().remoteAddress());
                ctx.close();
            }
        }
    }
}
