package org.example.GroupStorage;

import com.google.common.base.Charsets;
import org.apache.commons.cli.*;
import org.example.GroupStorage.peer.PeerHandler;
import org.example.GroupStorage.peer.PeerRunner;
import org.example.GroupStorage.peer.config.Configuration;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class Application {
    private final static Logger logger = LoggerFactory.getLogger(Application.class);

    private static String peerName;
    private static int bindPort;
    private static Configuration configuration;

    public static void main(String[] args) throws ParseException, IOException, InterruptedException {
        parseCliArguments(args);
        final PeerRunner peerRunner = createPeerRunner();
        peerRunner.run();

        String line;
        final BufferedReader reader = new BufferedReader(new InputStreamReader(System.in, Charsets.UTF_8));
        logger.info("Please enter a command");
        while ((line = reader.readLine()) != null) {
            logger.info("line: {}", line);
            final PeerRunner.CommandResult result = peerRunner.handleCommand(line);
            if (result == PeerRunner.CommandResult.SHUT_DOWN) {
                break;
            } else if (result == PeerRunner.CommandResult.INVALID_COMMAND && line.length() > 0) {
               logger.error("command not found");
            }
        }
    }

    private static Options createCliOptions() {
        Options options = new Options();
        Option name = new Option("n", "name", true, "Node name MUST be unique");
        name.setRequired(true);
        Option port = new Option("p","port", true, "Port binding");
        port.setRequired(true);
        Option help = new Option("h", "help", false,"display available commands");
        options.addOption(name);
        options.addOption(port);
        options.addOption(help);
        return options;
    }

    private static void parseCliArguments(String[] args) throws ParseException, IOException {
        HelpFormatter helpFormatter = new HelpFormatter();
        Options options = createCliOptions();
        CommandLineParser parser = new DefaultParser();
        CommandLine cmd = parser.parse(options, args);
        configuration = Configuration.initialize();

        if (cmd.hasOption("help")) {
           helpFormatter.printHelp("ant", options);
        }

        peerName = cmd.getOptionValue("n");
        bindPort = Integer.parseInt(cmd.getOptionValue("p"));
        logger.info("Entered options: {}, {}", peerName, bindPort);
    }

    private static PeerRunner createPeerRunner() {
        configuration.getNode().setPeerName(peerName);
        logger.info("Creating peer runner with the name: {}", configuration.getNode().getPeerName());
        return new PeerRunner(configuration, bindPort);
    }

    private static int createRandomInt(int max, int min) {
        return (int) (Math.random() * ((max - min) + 1)) + min;
    }

    public static void print(String out) {
        System.out.println(out);
    }
}
